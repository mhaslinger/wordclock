import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 * 
 * @author marce.at
 *
 */
public class WordClock
  implements ActionListener
{
  private JFrame frmWordClock;
  private JLabel label;

  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          WordClock window = new WordClock();
          window.frmWordClock.setVisible(true);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  public WordClock()
  {
    initialize();
    run();
  }

  private void run() {
    actionPerformed(null);
    Timer timer = new Timer(1000, this);
    timer.start();
  }

  private void initialize()
  {
    this.frmWordClock = new JFrame();
    this.frmWordClock.setTitle("Word Clock 1.0");
    this.frmWordClock.setBounds(100, 100, 383, 67);
    this.frmWordClock.setDefaultCloseOperation(3);
    this.frmWordClock.getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

    this.label = new JLabel("text");
    this.label.setHorizontalAlignment(0);
    this.frmWordClock.getContentPane().add(this.label);
  }

  public void actionPerformed(ActionEvent arg0)
  {
    String time = setCurTime();
    StringTokenizer st = new StringTokenizer(time, ":");

    int h = Integer.parseInt(st.nextToken());
    int m = Integer.parseInt(st.nextToken());
    this.label.setText(genDateText(h, m));
  }

  private String genDateText(int h, int m) {
    String[] t = { "mitternacht", "ein", "zwei", "drei", "vier", "fuenf", "sechs", "sieben", "acht", "neun", "zehn", "elf", "zwoelf" };
    String vor = "vor";
    String nach = "nach";
    String viertel = "viertel";
    String pm = "| pm";
    boolean pmflag = false;
    boolean hFlag = false;
    String min = null;
    String hr = null;
    String vn = null;
    if (h > 12) { h -= 12; pmflag = true; }
    if ((m < 18) || ((33 < m) && (m < 43))) vn = nach;
    else if ((m < 28) || (43 <= m)) vn = vor;
    if (m < 18) hr = t[h]; else hr = t[(h + 1)];
    if ((3 <= m) && (m < 8)) min = t[5];
    if ((8 <= m) && (m < 13)) min = t[10];
    if ((13 <= m) && (m < 18)) min = viertel;
    if ((18 <= m) && (m < 23)) min = t[10];
    if ((23 <= m) && (m < 28)) min = t[5];
    if ((28 <= m) && (m < 33)) min = "";
    if ((33 <= m) && (m < 38)) min = t[5];
    if ((38 <= m) && (m < 43)) min = t[10];
    if ((43 <= m) && (m < 48)) min = viertel;
    if ((48 <= m) && (m < 53)) min = t[10];
    if ((53 <= m) && (m < 58)) min = t[5];
    if ((58 <= m) || (m < 3)) return "Es ist genau " + hr + " Uhr";
    if ((18 <= m) && (m <= 42)) hFlag = true; else hFlag = false;
    StringBuilder ret = new StringBuilder("Es ist ");
    ret.append(min);
    if ((min.equals("fuenf")) || (min.equals("zehn"))) ret.append(" Minuten "); else ret.append(" ");
    if (vn != null) { ret.append(vn); ret.append(" "); }
    if (hFlag) ret.append("halb ");
    ret.append(hr);
    ret.append(" Uhr ");
    if (pmflag) { ret.append(pm); ret.append(" "); }
    String realtime = "| (";
    if(h<10){realtime+="0";};
    realtime += h+":";
    if (m<10){realtime+="0";};
    realtime += m + ")";
    ret.append(realtime);
    return ret.toString();
  }

  private String setCurTime() {
    Date now = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    String ret = sdf.format(now);
    return ret;
  }
}